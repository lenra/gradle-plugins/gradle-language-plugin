package fr.lenra.gradle.language;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.gradle.api.Project;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.Assert;
import org.junit.Test;

import fr.lenra.gradle.language.internal.DefaultLanguage;

public class LanguageTest {

	public static final LanguageValues[] languageOK = new LanguageValues[] {
		new LanguageValues("Java", "java", "java"),
		new LanguageValues("JavaScript", "javascript", "js"),
		new LanguageValues("ActionScript", "actionscript", "as"),
		new LanguageValues("GO", "go", "go"),
		new LanguageValues("C++", "cpp", "cpp"),
		new LanguageValues("C#", "cs", "cs")
	};
	
	@Test
	public void testNameGeneration() {
		Project project = ProjectBuilder.builder().build();
		DefaultLanguage.setProject(project);
		for (LanguageValues vals : languageOK) {
			Language l = DefaultLanguage.createLanguage(vals.getName());
			Assert.assertEquals("The language display name is not correct", vals.getName(), l.getDisplayName());
			Assert.assertEquals("The language name is not correct", vals.getSourceSetName(), l.getName());
			Assert.assertEquals("The language sourceSet name is not correct", vals.getSourceSetName(), l.getSourceDir());
			Assert.assertEquals("The language extension is not correct", vals.getExtension(), l.getExtension());
		}
	}

	@Test
	public void testLanguageSerialization() throws IOException {
		Project project = ProjectBuilder.builder().build();
		project.setGroup("fr.lenra.test");
		DefaultLanguage.setProject(project);
		DefaultLanguage l = DefaultLanguage.createLanguage("Java");
		l.setExtension("test");
		l.getPlugin().setName("java");

		ObjectMapper objectMapper = new ObjectMapper();
		
		String json = objectMapper.writeValueAsString(l);

		DefaultLanguage l2 = objectMapper.readValue(json, DefaultLanguage.class);

		Assert.assertEquals(l, l2);
	}


	private static class LanguageValues {
		private String name;
		private String sourceSetName;
		private String extension;

		public LanguageValues(String name, String sourceSetName, String extension) {
			this.name = name;
			this.sourceSetName = sourceSetName;
			this.extension = extension;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @return the sourceSetName
		 */
		public String getSourceSetName() {
			return sourceSetName;
		}

		/**
		 * @return the extension
		 */
		public String getExtension() {
			return extension;
		}
	}
}