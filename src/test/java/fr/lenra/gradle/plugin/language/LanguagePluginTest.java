package fr.lenra.gradle.plugin.language;

import static org.junit.Assert.assertNotNull;

import org.gradle.api.Project;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.Test;

public class LanguagePluginTest {

    @Test
	public void testApply() { 
        Project project = ProjectBuilder.builder().build();
        project.getPluginManager().apply(LanguagePlugin.class);

        assertNotNull(project.getTasks().getAt("binaries"));
    }
}