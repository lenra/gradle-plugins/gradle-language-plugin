package fr.lenra.gradle.plugin.language.internal

import java.io.BufferedReader
import java.io.InputStreamReader

import org.gradle.api.internal.project.ProjectInternal
import org.gradle.jvm.tasks.Jar
import org.gradle.plugin.devel.GradlePluginDevelopmentExtension

import fr.lenra.gradle.language.LanguageContainer
import fr.lenra.gradle.plugin.language.LanguagePlugin
import fr.lenra.gradle.language.internal.DefaultLanguageContainer
import fr.lenra.gradle.plugin.language.LanguagePluginPluginConvention

class DefaultLanguagePluginPluginConvention extends LanguagePluginPluginConvention {
	def languages;

	DefaultLanguagePluginPluginConvention(ProjectInternal project) {
		languages = project.getObjects().newInstance(DefaultLanguageContainer.class, project)
		languages.all {language ->
			project.extensions.gradlePlugin.plugins {
				register(language.name, {a ->
					id = language.plugin.name
					displayName = "${language.displayName} Plugin"
					description = "${language.displayName} language plugin"
					implementationClass = LanguagePlugin.class.name
				})
			}
		}

		def version = new BufferedReader(new InputStreamReader(DefaultLanguagePluginPluginConvention.class.getResourceAsStream("/.version")))
			.lines()
			.findFirst()
			.get()
		
		project.configurations {
			include
		}
		project.configurations.compile.extendsFrom(project.configurations.include)
		project.dependencies {
			include "fr.lenra.gradle:gradle-language-plugin:$version"
		}

		project.jar {
			from {
				project.configurations.include.collect {
					it.isDirectory() ? it : project.zipTree(it)
				}
			}
		}
	}
	
	@Override
	public LanguageContainer getLanguages() {
		return languages;
	}

}