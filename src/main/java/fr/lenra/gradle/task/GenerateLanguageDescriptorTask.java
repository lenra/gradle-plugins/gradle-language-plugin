package fr.lenra.gradle.task;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.gradle.api.DefaultTask;
import org.gradle.api.Project;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.TaskExecutionException;

import fr.lenra.gradle.language.Language;

public class GenerateLanguageDescriptorTask extends DefaultTask {

	@Input
	private Language language;

	/**
	 * @return the language
	 */
	public Language getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(Language language) {
		this.language = language;
	}

	@OutputFile
	private File output;

	/**
	 * @return the output
	 */
	public File getOutput() {
		return output;
	}

	/**
	 * @param output the output to set
	 */
	public void setOutput(File output) {
		this.output = output;
	}
   
	@TaskAction
	protected void generatePlugin() throws ClassNotFoundException {
		Project project = this.getProject();
		
		ObjectMapper objectMapper = new ObjectMapper();

		project.getLogger().info("Generates : "+output.getAbsolutePath());

		try {
			objectMapper.writeValue(output, language);
		}
		catch (IOException e) {
			project.getLogger().error("Failed to generate the language descriptor file : {}", output.getAbsolutePath());
			throw new TaskExecutionException(this, e);
		}
	}
}