package fr.lenra.gradle.task;

import java.io.File;

import org.gradle.api.file.FileCollection;
import org.gradle.api.file.SourceDirectorySet;
import org.gradle.api.provider.Property;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.CompileClasspath;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.OutputDirectory;
import org.gradle.api.tasks.SourceTask;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.incremental.IncrementalTaskInputs;

import fr.lenra.gradle.language.Language;
import fr.lenra.gradle.sourceset.LanguageSourceSet;
import fr.lenra.gradle.sourceset.internal.DefaultLanguageSourceSet;

/**
 * AbsctractLanguageCompileTask
 */
public abstract class AbsctractLanguageCompileTask extends SourceTask {
	private final Property<File> destinationDir;
	private FileCollection classpath;

	private LanguageSourceSet sourceSet;

	@Internal
	private Language language;

	public AbsctractLanguageCompileTask() {
		this.destinationDir = getProject().getObjects().property(File.class);
	}

	public final void init(LanguageSourceSet sourceSet, Language language) {
		DefaultLanguageSourceSet dSourceSet = (DefaultLanguageSourceSet) sourceSet;
		SourceDirectorySet sds = dSourceSet.getByName(language.getSourceDir());
		this.sourceSet = sourceSet;
		this.language = language;
		// this.setClasspath(sourceSet.getCompileClasspath());
		this.setSource(sds);
		this.setDestinationDir(sds.getOutputDir());
	}

	@Internal
	public LanguageSourceSet getLanguageSourceSet() {
		return this.sourceSet;
	}

	/**
	 * @return the language
	 */
	public Language getLanguage() {
		return language;
	}

	@TaskAction
	protected void compile(IncrementalTaskInputs inputs) {
		compile();
	}

	protected abstract void compile();

	// /**
	//  * Returns the classpath to use to compile the source files.
	//  *
	//  * @return The classpath.
	//  */
    // @CompileClasspath
	// public FileCollection getClasspath() {
	// 	return classpath;
	// }

	// /**
	//  * Sets the classpath to use to compile the source files.
	//  *
	//  * @param configuration The classpath. Must not be null, but may be empty.
	//  */
	// public void setClasspath(FileCollection configuration) {
	// 	this.classpath = configuration;
	// }

	/**
	 * Returns the directory to generate the binaries files into.
	 *
	 * @return The destination directory.
	 */
	@OutputDirectory
	public File getDestinationDir() {
		return destinationDir.getOrNull();
	}

	/**
	 * Sets the directory to generate the binaries files into.
	 *
	 * @param destinationDir The destination directory. Must not be null.
	 */
	public void setDestinationDir(File destinationDir) {
		this.destinationDir.set(destinationDir);
	}

	/**
	 * Sets the directory to generate the binaries files into.
	 *
	 * @param destinationDir The destination directory. Must not be null.
	 */
	public void setDestinationDir(Provider<File> destinationDir) {
		this.destinationDir.set(destinationDir);
	}
}