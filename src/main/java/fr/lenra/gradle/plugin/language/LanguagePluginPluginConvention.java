package fr.lenra.gradle.plugin.language;

import fr.lenra.gradle.language.LanguageContainer;

public abstract class LanguagePluginPluginConvention {
	public abstract LanguageContainer getLanguages();
}