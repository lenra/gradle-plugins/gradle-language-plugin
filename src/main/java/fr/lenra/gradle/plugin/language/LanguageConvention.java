package fr.lenra.gradle.plugin.language;

import org.gradle.api.Project;

import fr.lenra.gradle.language.Language;

public abstract class LanguageConvention {

	private final Project project;
	private final Language language;
	private final LanguageExtension extension;

	public LanguageConvention(Project project, Language language, LanguageExtension extension) {
		this.project = project;
		this.language = language;
		this.extension = extension;
	}

	public Project getProject() {
		return project;
	}

	public Language getLanguage() {
		return language;
	}

	public LanguageExtension getExtension() {
		return extension;
	}

	public abstract void configure();
}