package fr.lenra.gradle.plugin.language;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.gradle.api.Plugin;
import org.gradle.api.internal.project.ProjectInternal;
import org.gradle.api.plugins.BasePlugin;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import fr.lenra.gradle.language.Language;
import fr.lenra.gradle.language.LanguageContainer;
import fr.lenra.gradle.language.internal.DefaultLanguage;
import fr.lenra.gradle.language.internal.DefaultLanguageContainer;
import fr.lenra.gradle.plugin.language.internal.DefaultLanguagePluginConvention;
import fr.lenra.gradle.sourceset.LanguageSourceSetContainer;

public class LanguagePlugin implements Plugin<ProjectInternal> {
	public static final String LANGUAGES_DIRECTORY = "/META-INF/gradle-languages/";
	public static final String PLUGIN_CONVENTION_NAME = "language";

	protected LanguageContainer languages;
	protected ProjectInternal project;

	@Override
	public void apply(ProjectInternal project) {
		this.project = project;
		project.getPluginManager().apply(BasePlugin.class);

		LanguagePluginConvention pluginConvention = (LanguagePluginConvention) project.getConvention().getPlugins().get(PLUGIN_CONVENTION_NAME);
		// define the convention
		if (pluginConvention==null) {
			pluginConvention = project.getObjects().newInstance(DefaultLanguagePluginConvention.class, project, project.getObjects().newInstance(DefaultLanguageContainer.class, project));
			project.getConvention().getPlugins().put(PLUGIN_CONVENTION_NAME, pluginConvention);
		}
		languages = pluginConvention.getLanguages();

		// define the source set
		if (project.getExtensions().findByName("sourceSets")==null)
			project.getExtensions().add(LanguageSourceSetContainer.class, "sourceSets", pluginConvention.getSourceSets());

		loadLanguages();
	}

	protected void loadLanguages() {
		project.getLogger().info("Load languages");
		ObjectMapper objectMapper = new ObjectMapper();
		DefaultLanguage.setProject(this.project);

		try {
			ClassLoader cl = this.getClass().getClassLoader();
			ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(cl);
			Resource[] resources = resolver.getResources("classpath*:"+LANGUAGES_DIRECTORY+"*.lang");
			for (Resource resource: resources){
				project.getLogger().debug("Loading the language file "+resource.getFilename());
				Language l = objectMapper.readValue(resource.getInputStream(), DefaultLanguage.class);
				if (languages.findByName(l.getName())==null)
					languages.add(l);
			}
		}
		catch (IOException e) {
			throw new RuntimeException("Error while loading the languages", e);
		}
	}
}