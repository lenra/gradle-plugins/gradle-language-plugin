package fr.lenra.gradle.plugin.language;

import org.gradle.api.Project;

import fr.lenra.gradle.language.Language;

public abstract class LanguageExtension {

	private final Project project;
	private final Language language;

	public LanguageExtension(Project project, Language language) {
		this.project = project;
		this.language = language;
	}

	public Project getProject() {
		return project;
	}

	public Language getLanguage() {
		return language;
	}
}