package fr.lenra.gradle.plugin.language;

import java.io.File;

import org.gradle.api.Plugin;
import org.gradle.api.Task;
import org.gradle.api.internal.project.ProjectInternal;
import org.gradle.api.plugins.JavaPluginConvention;
import org.gradle.api.plugins.ReportingBasePlugin;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.SourceSetContainer;
import org.gradle.plugin.devel.plugins.JavaGradlePluginPlugin;
import org.gradle.util.GUtil;

import fr.lenra.gradle.language.LanguageContainer;
import fr.lenra.gradle.plugin.language.internal.DefaultLanguagePluginPluginConvention;
import fr.lenra.gradle.task.GenerateLanguageDescriptorTask;

public class LanguagePluginPlugin implements Plugin<ProjectInternal> {

	public static final String PLUGIN_NAME = "language-plugin";

	@Override
	public void apply(ProjectInternal project) {
		project.getPluginManager().apply(JavaGradlePluginPlugin.class);
		project.getPluginManager().apply(ReportingBasePlugin.class);

		project.getRepositories().mavenLocal();

		LanguagePluginPluginConvention convention = new DefaultLanguagePluginPluginConvention(project);
		project.getConvention().getPlugins().put(PLUGIN_NAME, convention);

		// Add language container
		project.getExtensions().add(LanguageContainer.class, "languages", convention.getLanguages());

		
		// Creatte language descriptor tasks
		JavaPluginConvention javaPlugin = project.getConvention().getPlugin(JavaPluginConvention.class);
		SourceSetContainer sourceSets = javaPlugin.getSourceSets();
		SourceSet main = sourceSets.findByName("main");

		File descriptorsDir = new File(main.getOutput().getResourcesDir(), LanguagePlugin.LANGUAGES_DIRECTORY);
		descriptorsDir.mkdirs();

		Task descriptors = project.getTasks().maybeCreate("generateLanguageDescriptors");
		descriptors.mustRunAfter(project.getTasks().getByName("processResources"));
		project.getTasks().getByName("classes").dependsOn(descriptors);

		convention.getLanguages().all(l -> {
			// Create tasks
			GenerateLanguageDescriptorTask t = project.getTasks().maybeCreate(
					GUtil.toLowerCamelCase("generate " + l.getName() + " descriptor"),
					GenerateLanguageDescriptorTask.class);
			t.setLanguage(l);
			t.setOutput(new File(descriptorsDir, l.getName()+".lang"));
			descriptors.dependsOn(t);
		});
	}
}