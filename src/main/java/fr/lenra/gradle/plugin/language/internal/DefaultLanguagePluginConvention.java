package fr.lenra.gradle.plugin.language.internal;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.artifacts.ConfigurationContainer;
import org.gradle.api.component.AdhocComponentWithVariants;
import org.gradle.api.component.SoftwareComponentFactory;
import org.gradle.api.file.SourceDirectorySet;
import org.gradle.api.internal.project.ProjectInternal;
import org.gradle.api.plugins.BasePlugin;
import org.gradle.api.tasks.Copy;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.TaskContainer;
import org.gradle.api.tasks.testing.AbstractTestTask;
import org.gradle.util.GUtil;

import fr.lenra.gradle.language.Language;
import fr.lenra.gradle.language.LanguageContainer;
import fr.lenra.gradle.language.internal.DefaultLanguageContainer;
import fr.lenra.gradle.plugin.language.LanguageConvention;
import fr.lenra.gradle.plugin.language.LanguageExtension;
import fr.lenra.gradle.plugin.language.LanguagePluginConvention;
import fr.lenra.gradle.sourceset.LanguageSourceSetContainer;
import fr.lenra.gradle.sourceset.internal.DefaultLanguageSourceSet;
import fr.lenra.gradle.sourceset.internal.DefaultLanguageSourceSetContainer;
import fr.lenra.gradle.task.AbsctractLanguageCompileTask;
import groovy.lang.Closure;

public class DefaultLanguagePluginConvention extends LanguagePluginConvention {
	private static final String VERIFICATION_TASKS_GROUP = "Verification";
	private static final String IMPLEMENTATION_CONFIGURATION_NAME = "implementation";
	private static final String COMPILE_ONLY_CONFIGURATION_NAME = "compileOnly";
	private static final String RUNTIME_ONLY_CONFIGURATION_NAME = "runtimeOnly";
	private static final String COMILE_CLASSPATH_NAME = "compileClasspath";
	private static final String RUNTIME_CLASSPATH_NAME = "runtimeClasspath";

	private final Project project;
	private DefaultLanguageSourceSetContainer sourceSets;
	private LanguageContainer languages;
	private Map<Language, LanguageExtension> extensions = new HashMap<>();
	private Map<Language, LanguageConvention> conventions = new HashMap<>();
	private SoftwareComponentFactory softwareComponentFactory;

	@Inject
	public DefaultLanguagePluginConvention(ProjectInternal project, DefaultLanguageContainer languages, SoftwareComponentFactory softwareComponentFactory) {
		this.project = project;
		this.languages = languages;
        this.softwareComponentFactory = softwareComponentFactory;

		this.defineLanguageExtensions();
		this.sourceSets = project.getObjects().newInstance(DefaultLanguageSourceSetContainer.class);
		this.sourceSets.init(project, languages);
		this.configureSourceSets();
	}

	@Override
	public Project getProject() {
		return project;
	}

	/**
	 * @return the languages
	 */
	public LanguageContainer getLanguages() {
		return languages;
	}

	public Object sourceSets(Closure<LanguageSourceSetContainer> closure) {
		return sourceSets.configure(closure);
	}

	@Override
	public LanguageSourceSetContainer getSourceSets() {
		return this.sourceSets;
	}

	@Override
	public LanguageExtension getLanguageExtension(final Language language) {
		return extensions.get(language);
	}

	@Override
	public LanguageConvention getLanguageConvention(final Language language) {
		return conventions.get(language);
	}

	private void configureSourceSets() {
		this.getSourceSets().maybeCreate("main");
		this.getSourceSets().maybeCreate("test");
		this.defineBaseTasks();
		this.getSourceSets().all(sourceSet -> {
			DefaultLanguageSourceSet defSourceSet = (DefaultLanguageSourceSet) sourceSet;
			defineSourceDirectories(defSourceSet);
			defineConfigurations(defSourceSet);
			defineTasks(defSourceSet);
		});
	}

	private void defineSourceDirectories(DefaultLanguageSourceSet sourceSet) {
		project.getLogger().info("Sources directories creation for {}", sourceSet.getName());
		this.languages.all(language -> {
			project.getLogger().debug("Create sources directory for {} : {}", language.getDisplayName(), language.getSourceDir());
			SourceDirectorySet dirSet = sourceSet.create(language);
			dirSet.getFilter().include("**/*." + language.getExtension());
			dirSet.srcDir(new File(project.getRootDir(), "src/" + sourceSet.getName() + "/" + language.getSourceDir()));
		});
	}

	private void defineConfigurations(DefaultLanguageSourceSet sourceSet) {
		String sourceSetName = sourceSet.getName();
		ConfigurationContainer configurations = this.project.getConfigurations();

		String configName = sourceSet.getConfigName(IMPLEMENTATION_CONFIGURATION_NAME);
		Configuration implementationConfiguration = configurations.maybeCreate(configName);
		implementationConfiguration.setDescription("Implementation dependencies for " + sourceSetName + ".");
		implementationConfiguration.setVisible(false);
		implementationConfiguration.setCanBeConsumed(false);
		implementationConfiguration.setCanBeResolved(false);

		configName = sourceSet.getConfigName(COMPILE_ONLY_CONFIGURATION_NAME);
		Configuration compileOnlyConfiguration = configurations.maybeCreate(configName);
		compileOnlyConfiguration.setDescription("Compile only dependencies for " + sourceSetName + ".");
		compileOnlyConfiguration.setVisible(false);
		compileOnlyConfiguration.setCanBeConsumed(false);
		compileOnlyConfiguration.setCanBeResolved(false);

		configName = sourceSet.getConfigName(RUNTIME_ONLY_CONFIGURATION_NAME);
		Configuration runtimeOnlyConfiguration = configurations.maybeCreate(configName);
		runtimeOnlyConfiguration.setDescription("Runtime only dependencies for " + sourceSetName + ".");
		runtimeOnlyConfiguration.setVisible(false);
		runtimeOnlyConfiguration.setCanBeConsumed(false);
		runtimeOnlyConfiguration.setCanBeResolved(false);

		// Classpaths
		configName = sourceSet.getConfigName(COMILE_CLASSPATH_NAME);
		Configuration compileClasspathConfiguration = configurations.maybeCreate(configName);
		compileClasspathConfiguration.setDescription("Compile classpath for " + sourceSetName + ".");
		compileClasspathConfiguration.setVisible(false);
		compileClasspathConfiguration.setCanBeConsumed(false);
		compileClasspathConfiguration.setCanBeResolved(true);

		configName = sourceSet.getConfigName(RUNTIME_CLASSPATH_NAME);
		Configuration runtimeClasspathConfiguration = configurations.maybeCreate(configName);
		runtimeClasspathConfiguration.setDescription("Runtime classpath of " + sourceSetName + ".");
		runtimeClasspathConfiguration.setVisible(false);
		runtimeClasspathConfiguration.setCanBeConsumed(false);
		runtimeClasspathConfiguration.setCanBeResolved(true);

		// Add the classpaths to the sourceSet
		sourceSet.setCompileClasspath(compileClasspathConfiguration);
		sourceSet.setRuntimeClasspath(sourceSet.getOutput().plus(runtimeClasspathConfiguration));

        // compileOnlyConfiguration.deprecateForConsumption(apiElementsConfigurationName);
        // compileOnlyConfiguration.deprecateForResolution(compileClasspathConfiguration.getName());

        // runtimeConfiguration.deprecateForDeclaration(runtimeOnlyConfigurationName);
        // runtimeConfiguration.deprecateForConsumption(runtimeElementsConfigurationName);
        // runtimeConfiguration.deprecateForResolution(runtimeClasspathConfigurationName);

        // compileClasspathConfiguration.deprecateForDeclaration(implementationConfigurationName, compileOnlyConfigurationName);
        // runtimeClasspathConfiguration.deprecateForDeclaration(implementationConfigurationName, compileOnlyConfigurationName, runtimeOnlyConfigurationName);

		// Extends 
		compileClasspathConfiguration.extendsFrom(compileOnlyConfiguration, implementationConfiguration);
		runtimeClasspathConfiguration.extendsFrom(runtimeOnlyConfiguration, implementationConfiguration);
		if ("main".equals(sourceSetName)) {
			configurations.getAt("default").extendsFrom(runtimeClasspathConfiguration);
		}
		else if ("test".equals(sourceSetName)) {
			implementationConfiguration.extendsFrom(configurations.getByName(IMPLEMENTATION_CONFIGURATION_NAME));
			compileOnlyConfiguration.extendsFrom(configurations.getByName(COMPILE_ONLY_CONFIGURATION_NAME));
			runtimeOnlyConfiguration.extendsFrom(configurations.getByName(RUNTIME_ONLY_CONFIGURATION_NAME));
		}
	}

	@SuppressWarnings("unchecked")
	private void defineBaseTasks() {
		TaskContainer taskContainer = project.getTasks();
		Task check = taskContainer.getByName("check");
		Task test = taskContainer.maybeCreate("test");
		test.setGroup(VERIFICATION_TASKS_GROUP);
		test.setDescription("Runs the unit tests.");
		Task binaries = taskContainer.maybeCreate("binaries");

		final ClassLoader classLoader = DefaultLanguagePluginConvention.class.getClassLoader();
		this.languages.all(language -> {
			try {
				Class<? extends AbstractTestTask> testTaskClass = (Class<? extends AbstractTestTask>) classLoader
						.loadClass(language.getPlugin().getTestClass());
				String name = GUtil.toLowerCamelCase("test " + language.getName());
				project.getLogger().debug("Create compile task {}", name);
				AbstractTestTask tt = taskContainer.maybeCreate(name, testTaskClass);
				test.dependsOn(tt);
			} catch (ClassNotFoundException e) {
				project.getLogger().info("Task creation error : {} not found", language.getPlugin().getTestClass());
			}
		});
		
		test.dependsOn(binaries);
		check.dependsOn(test);
		taskContainer.getByName("build").dependsOn(check, taskContainer.getByName("assemble"));
	}

	@SuppressWarnings("unchecked")
	private void defineTasks(DefaultLanguageSourceSet sourceSet) {
		String sourceSetName = sourceSet.getName();
		project.getLogger().info("Tasks creation for {}", sourceSetName);
		String depName = sourceSetName.equals(SourceSet.MAIN_SOURCE_SET_NAME) ? "" : sourceSetName;
		TaskContainer taskContainer = project.getTasks();
		final ClassLoader classLoader = DefaultLanguagePluginConvention.class.getClassLoader();
		final Task testTask = taskContainer.getByName("test");
		String taskName = GUtil.toLowerCamelCase(depName + " binaries");
		Task binaries = taskContainer.maybeCreate(taskName);
		binaries.setGroup(BasePlugin.BUILD_GROUP);
		binaries.setDescription("Assembles "+sourceSetName+" binaries.");
		this.languages.all(language -> {
			try {
				Class<? extends AbsctractLanguageCompileTask> taskClass = 
					(Class<? extends AbsctractLanguageCompileTask>) classLoader.loadClass(language.getPlugin().getCompileClass());
				String name = GUtil.toLowerCamelCase("compile " + depName + " " + language.getName());
				project.getLogger().debug("Create compile task {}", name);
				AbsctractLanguageCompileTask t = taskContainer.maybeCreate(name, taskClass);
				t.init(sourceSet, language);
				t.setGroup(BasePlugin.BUILD_GROUP);
				t.setDescription("Assembles "+sourceSetName+" binaries for language "+language.getDisplayName()+".");
				if (sourceSet.getName().equals("test"))
					t.dependsOn(taskContainer.getByName("binaries"));

				binaries.dependsOn(t);
			} catch (ClassNotFoundException e) {
				project.getLogger().info("Task creation error : {} not found", language.getPlugin().getCompileClass());
			}
		});

		taskName = GUtil.toLowerCamelCase("process " + depName + " resources");
		project.getLogger().debug("Create task {}", taskName);
		Copy processResources = taskContainer.maybeCreate(taskName, Copy.class);
		processResources.from(sourceSet.getResources());
		processResources.setDestinationDir(sourceSet.getResources().getOutputDir());
		binaries.dependsOn(processResources);
		testTask.dependsOn(binaries);
	}

	@SuppressWarnings("unchecked")
	private void defineLanguageExtensions() {
		final ClassLoader classLoader = DefaultLanguagePluginConvention.class.getClassLoader();
		this.languages.all(l -> {
			// Create language component variant container
			AdhocComponentWithVariants adhoc = softwareComponentFactory.adhoc(l.getName());
			project.getComponents().add(adhoc);

			String c = l.getPlugin().getExtensionClass();
			LanguageExtension extension = null;
			if (c!=null) {
				try {
					Class<? extends LanguageExtension> extensionClass = 
						(Class<? extends LanguageExtension>) classLoader.loadClass(c);

					extension = project.getObjects().newInstance(extensionClass, project, l);
					extensions.put(l, extension);
					project.getExtensions().add(l.getName(), extension);
				} catch (ClassNotFoundException e) {
					project.getLogger().info("Extension creation error : {} not found", c);
				}
			}
			
			c = l.getPlugin().getConventionClass();
			if (c!=null) {
				try {
					Class<? extends LanguageConvention> conventionClass = 
						(Class<? extends LanguageConvention>) classLoader.loadClass(c);

					LanguageConvention convention = project.getObjects().newInstance(conventionClass, project, l, extension);
					conventions.put(l, convention);
					project.getConvention().getPlugins().put(l.getName(), convention);
					convention.configure();
				} catch (ClassNotFoundException e) {
					project.getLogger().info("Convention creation error : {} not found", c);
				}
			}
		});
	}
}