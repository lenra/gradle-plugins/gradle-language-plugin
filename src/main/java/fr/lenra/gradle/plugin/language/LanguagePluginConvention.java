package fr.lenra.gradle.plugin.language;

import org.gradle.api.Project;

import fr.lenra.gradle.language.Language;
import fr.lenra.gradle.language.LanguageContainer;
import fr.lenra.gradle.sourceset.LanguageSourceSetContainer;

public abstract class LanguagePluginConvention {

	public abstract Project getProject();

	public abstract LanguageContainer getLanguages();

	public abstract LanguageSourceSetContainer getSourceSets();

	public abstract LanguageConvention getLanguageConvention(Language language);

	public abstract LanguageExtension getLanguageExtension(Language language);
}