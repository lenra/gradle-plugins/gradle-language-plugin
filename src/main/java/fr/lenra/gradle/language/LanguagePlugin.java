package fr.lenra.gradle.language;

public interface LanguagePlugin {

	public String getName();

	public void setName(String name);

	public default String name(String name) {
		this.setName(name);
		return this.getName();
	}

	public boolean isBase();

	public void setBase(boolean base);

	public default boolean base(boolean base) {
		this.setBase(base);
		return this.isBase();
	}

	public String getConventionClass();

	public void setConventionClass(String className);

	public default String conventionClass(String className) {
		this.setConventionClass(className);
		return this.getConventionClass();
	}

	public String getExtensionClass();

	public void setExtensionClass(String className);

	public default String configurationClass(String className) {
		this.setExtensionClass(className);
		return this.getExtensionClass();
	}

	public String getCompileClass();

	public void setCompileClass(String className);

	public default String compileClass(String className) {
		this.setCompileClass(className);
		return this.getCompileClass();
	}

	public String getTestClass();

	public void setTestClass(String className);

	public default String testClass(String className) {
		this.setTestClass(className);
		return this.getTestClass();
	}

	public String getDocumentationClass();

	public void setDocumentationClass(String className);

	public default String documentationClass(String className) {
		this.setDocumentationClass(className);
		return this.getDocumentationClass();
	}

	public default String docClass(String className) {
		return this.documentationClass(className);
	}
}