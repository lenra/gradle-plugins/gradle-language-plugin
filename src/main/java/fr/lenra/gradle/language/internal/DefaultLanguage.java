package fr.lenra.gradle.language.internal;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.gradle.api.Project;
import org.gradle.api.provider.Property;
import org.gradle.api.provider.Provider;

import fr.lenra.gradle.language.Language;
import fr.lenra.gradle.language.LanguagePlugin;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DefaultLanguage implements Language, Serializable {
	private static final Map<Character, Character> specialChars = new HashMap<Character, Character>() {
		private static final long serialVersionUID = 1L;
		{
			put('+', 'P');
			put('#', 'S');
		}
	};
	static Project project = null;
	
	@JsonIgnore
	final transient Provider<String> displayName;
	@JsonIgnore
	final transient Provider<String> normalizedName;
	@JsonIgnore
	final transient Property<String> name = project.getObjects().property(String.class);
	@JsonIgnore
	final transient Property<String> sourceDir = project.getObjects().property(String.class);
	@JsonIgnore
	final transient Property<String> extension = project.getObjects().property(String.class);
	@JsonIgnore
	transient DefaultLanguagePlugin plugin;

	private DefaultLanguage(String name) {
		// TODO: check the name pattern
		this.displayName = project.getProviders().provider(() -> name);

		this.normalizedName = this.displayName.map(n -> {
			Set<Entry<Character, Character>> entries = specialChars.entrySet();
			for (Entry<Character, Character> c : entries) {
				n = n.replaceAll("[" + c.getKey() + "]", "" + c.getValue());
			}
			return n;
		});

		this.name.convention(this.normalizedName.map(String::toLowerCase));
		
		this.sourceDir.convention(this.normalizedName.map(String::toLowerCase));
		
		
		this.extension.convention(this.normalizedName.map(n -> {
			String uppercaseChars = n.replaceAll("[a-z]+", "");
			if (uppercaseChars.length() < 2)
				uppercaseChars = n;
			return uppercaseChars.toLowerCase();
		}));

		this.setPlugin(new DefaultLanguagePlugin());
	}

	@Override
	@JsonProperty
	public String getDisplayName() {
		return this.displayName.getOrNull();
	}

	@Override
	@JsonProperty
	public String getName() {
		return this.name.getOrNull();
	}

	@Override
	public void setName(String name) {
		this.name.set(name);
	}

	@Override
	@JsonProperty
	public String getSourceDir() {
		return sourceDir.getOrNull();
	}

	@Override
	public void setSourceDir(String sourceDir) {
		this.sourceDir.set(sourceDir);
	}

	@Override
	@JsonProperty
	public String getExtension() {
		return extension.getOrNull();
	}

	@Override
	public void setExtension(String extension) {
		this.extension.set(extension);
	}

	@Override
	@JsonProperty
	public LanguagePlugin getPlugin() {
		return this.plugin;
	}

	public void setPlugin(DefaultLanguagePlugin plugin) {
		this.plugin = plugin;
		this.plugin.language.set(this);
	}

	@Override
	public String toString() {
		return this.name.get();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Language) {
			Language l = (Language) obj;
			return l.getDisplayName().equals(this.getDisplayName()) && 
				l.getName().equals(this.getName()) && 
				l.getSourceDir().equals(this.getSourceDir()) && 
				l.getExtension().equals(this.getExtension()) && 
				l.getPlugin().equals(this.getPlugin());
		}
		return false;
	}

	@JsonCreator
	public static DefaultLanguage createLanguage(@JsonProperty("displayName") String displayName) {
		return new DefaultLanguage(displayName);
	}

	public static void setProject(Project project) {
		DefaultLanguage.project = project;
	}
}