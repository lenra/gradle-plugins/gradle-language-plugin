package fr.lenra.gradle.language.internal;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.gradle.api.provider.Property;

import fr.lenra.gradle.language.LanguagePlugin;


@JsonIgnoreProperties(ignoreUnknown = true)
public class DefaultLanguagePlugin implements LanguagePlugin {
	@JsonIgnore
	final transient Property<DefaultLanguage> language = DefaultLanguage.project.getObjects().property(DefaultLanguage.class);
	@JsonIgnore
	final transient Property<String> name = DefaultLanguage.project.getObjects().property(String.class);
	@JsonIgnore
	final transient Property<Boolean> base = DefaultLanguage.project.getObjects().property(Boolean.class);
	@JsonIgnore
	final transient Property<String> conventionClass = DefaultLanguage.project.getObjects().property(String.class);
	@JsonIgnore
	final transient Property<String> extensionClass = DefaultLanguage.project.getObjects().property(String.class);
	@JsonIgnore
	final transient Property<String> compileClass = DefaultLanguage.project.getObjects().property(String.class);
	@JsonIgnore
	final transient Property<String> testClass = DefaultLanguage.project.getObjects().property(String.class);
	@JsonIgnore
	final transient Property<String> documentationClass = DefaultLanguage.project.getObjects().property(String.class);

	@JsonCreator
	DefaultLanguagePlugin() {
		name.convention(language.flatMap(l -> l.name.flatMap(n -> {
			return base.map(b -> {
				StringBuilder sb = new StringBuilder();
				sb.append(DefaultLanguage.project.getGroup());
				sb.append('.');
				sb.append(n.toLowerCase());
				if (b)
					sb.append("-base");
				return sb.toString();
			});
		})));

		base.convention(false);
		
		conventionClass.convention(language.flatMap(l -> l.normalizedName.map(n -> {
			StringBuilder sb = new StringBuilder();
				sb.append(DefaultLanguage.project.getGroup());
				sb.append(".plugin.");
				sb.append(n);
				sb.append("PluginConvention");
			return sb.toString();
		})));
		
		extensionClass.convention(language.flatMap(l -> l.normalizedName.map(n -> {
			StringBuilder sb = new StringBuilder();
				sb.append(DefaultLanguage.project.getGroup());
				sb.append(".plugin.");
				sb.append(n);
				sb.append("Extension");
			return sb.toString();
		})));
		
		compileClass.convention(language.flatMap(l -> l.normalizedName.map(n -> {
			StringBuilder sb = new StringBuilder();
				sb.append(DefaultLanguage.project.getGroup());
				sb.append(".task.");
				sb.append(n);
				sb.append("Compile");
			return sb.toString();
		})));

		testClass.convention(language.flatMap(l -> l.normalizedName.map(n -> {
			StringBuilder sb = new StringBuilder();
				sb.append(DefaultLanguage.project.getGroup());
				sb.append(".task.");
				sb.append(n);
				sb.append("Test");
			return sb.toString();
		})));
	
		documentationClass.convention(language.flatMap(l -> l.normalizedName.map(n -> {
			StringBuilder sb = new StringBuilder();
				sb.append(DefaultLanguage.project.getGroup());
				sb.append(".task.");
				sb.append(n);
				sb.append("Documentation");
			return sb.toString();
		})));
	}

	@Override
	@JsonProperty
	public String getName() {
		return this.name.getOrNull();
	}

	@Override
	public void setName(String name) {
		this.name.set(name);
	}

	@Override
	@JsonProperty
	public boolean isBase() {
		return this.base.getOrElse(Boolean.FALSE);
	}

	@Override
	public void setBase(boolean base) {
		this.base.set(base);
	}

	@Override
	@JsonProperty
	public String getConventionClass() {
		return this.conventionClass.getOrNull();
	}

	@Override
	public void setConventionClass(String className) {
		this.conventionClass.set(className);
	}

	@Override
	@JsonProperty
	public String getExtensionClass() {
		return this.extensionClass.getOrNull();
	}

	@Override
	public void setExtensionClass(String className) {
		this.extensionClass.set(className);
	}

	@Override
	@JsonProperty
	public String getCompileClass() {
		return this.compileClass.getOrNull();
	}

	@Override
	public void setCompileClass(String className) {
		this.compileClass.set(className);
	}

	@Override
	@JsonProperty
	public String getTestClass() {
		return this.testClass.getOrNull();
	}

	@Override
	public void setTestClass(String className) {
		this.testClass.set(className);
	}

	@Override
	@JsonProperty
	public String getDocumentationClass() {
		return this.documentationClass.getOrNull();
	}

	@Override
	public void setDocumentationClass(String className) {
		this.documentationClass.set(className);
	}

	@Override
	public String toString() {
		return this.getName();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof LanguagePlugin) {
			LanguagePlugin l = (LanguagePlugin) obj;
			return l.getName().equals(this.getName()) && 
				l.isBase()==this.isBase() && 
				l.getCompileClass().equals(this.getCompileClass()) && 
				l.getTestClass().equals(this.getTestClass()) && 
				l.getDocumentationClass().equals(this.getDocumentationClass());
		}
		return false;
	}
}