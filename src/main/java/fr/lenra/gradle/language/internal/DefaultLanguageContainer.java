package fr.lenra.gradle.language.internal;

import javax.inject.Inject;

import org.gradle.api.Namer;
import org.gradle.api.internal.AbstractValidatingNamedDomainObjectContainer;
import org.gradle.api.internal.CollectionCallbackActionDecorator;
import org.gradle.api.internal.project.ProjectInternal;
import org.gradle.api.reflect.TypeOf;
import org.gradle.internal.reflect.Instantiator;

import fr.lenra.gradle.language.Language;
import fr.lenra.gradle.language.LanguageContainer;
import fr.lenra.gradle.sourceset.LanguageSourceSetContainer;

public class DefaultLanguageContainer extends AbstractValidatingNamedDomainObjectContainer<Language> implements LanguageContainer {

	@Inject
	public DefaultLanguageContainer(Instantiator instantiator, CollectionCallbackActionDecorator collectionCallbackActionDecorator, ProjectInternal project) {
		super(Language.class, instantiator, (Namer<? super Language>) new Namer<Language>() {
			public String determineName(Language ss) {
				return ss.getDisplayName();
			}
		}, collectionCallbackActionDecorator);
		DefaultLanguage.setProject(project);
	}

	@Override
	protected Language doCreate(String name) {
		return DefaultLanguage.createLanguage(name);
	}

	@Override
	public TypeOf<?> getPublicType() {
		return TypeOf.typeOf(LanguageSourceSetContainer.class);
	}
}