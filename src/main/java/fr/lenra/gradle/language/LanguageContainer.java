package fr.lenra.gradle.language;

import org.gradle.api.NamedDomainObjectContainer;

public interface LanguageContainer extends NamedDomainObjectContainer<Language> {
    
}