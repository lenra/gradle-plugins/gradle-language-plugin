package fr.lenra.gradle.language;

public interface Language {
	/**
	 * @return the language display name
	 */
	String getDisplayName();

	/**
	 * @param name the language name
	 */
	void setName(String name);

	/**
	 * @return the language name
	 */
	String getName();

	/**
	 * @return the sourceDir
	 */
	String getSourceDir();

	/**
	 * @param sourceDir the sourceDir to set
	 */
	void setSourceDir(String sourceDir);

	/**
	 * @return the extension
	 */
	String getExtension();

	/**
	 * @param extension the extension to set
	 */
	void setExtension(String extension);

	/**
	 * @return the generated plugin configuration
	 */
	LanguagePlugin getPlugin();
}