package fr.lenra.gradle.sourceset;

import org.gradle.api.NamedDomainObjectContainer;

public interface LanguageSourceSetContainer extends NamedDomainObjectContainer<LanguageSourceSet> {
    
}