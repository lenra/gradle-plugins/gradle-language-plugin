package fr.lenra.gradle.sourceset.internal;

import org.gradle.api.internal.file.CompositeFileCollection;
import org.gradle.api.internal.file.collections.FileCollectionResolveContext;
import org.gradle.api.internal.project.ProjectInternal;

import fr.lenra.gradle.sourceset.SourceSetOutput;

public class DefaultSourceSetOutput extends CompositeFileCollection implements SourceSetOutput {
	private DefaultBaseSourceSet sourceSet;
 
	public DefaultSourceSetOutput(ProjectInternal project, DefaultBaseSourceSet sourceSet) {
		project.getLogger().info("Create SourceSetOutput for "+sourceSet.getName());
		this.sourceSet = sourceSet;
	}

	@Override
	public String getDisplayName() {
		return this.sourceSet.getName()+" output";
	}

	@Override
	public void visitContents(FileCollectionResolveContext context) {
		sourceSet.all(sds -> {
			context.add(sds.getOutputDir());
		});
	}
}