package fr.lenra.gradle.sourceset.internal;

import java.io.File;
import java.util.Collection;

import javax.inject.Inject;

import org.gradle.api.artifacts.Configuration;
import org.gradle.api.file.FileCollection;
import org.gradle.api.file.SourceDirectorySet;
import org.gradle.api.internal.CollectionCallbackActionDecorator;
import org.gradle.api.internal.project.ProjectInternal;
import org.gradle.api.model.ObjectFactory;
import org.gradle.api.reflect.TypeOf;
import org.gradle.api.tasks.SourceSet;
import org.gradle.internal.reflect.Instantiator;
import org.gradle.platform.base.internal.ComponentSpecIdentifier;
import org.gradle.util.GUtil;

import fr.lenra.gradle.language.Language;
import fr.lenra.gradle.sourceset.LanguageSourceSet;

public class DefaultLanguageSourceSet extends DefaultResourcesSourceSet implements LanguageSourceSet {
	public ComponentSpecIdentifier componentIdentifier;
	public ObjectFactory factory;
	private Configuration compileClasspath;
	private FileCollection runtimeClasspath;

	@Inject
	public DefaultLanguageSourceSet(String name, Collection<Language> languages, ProjectInternal project, Instantiator instantiator, CollectionCallbackActionDecorator collectionCallbackActionDecorator) {
		super(name, project, instantiator, collectionCallbackActionDecorator);
	}

	public Configuration getCompileClasspath() {
		return compileClasspath;
	}

	public void setCompileClasspath(Configuration compileClasspath) {
		this.compileClasspath = compileClasspath;
	}

	public FileCollection getRuntimeClasspath() {
		return runtimeClasspath;
	}

	public void setRuntimeClasspath(FileCollection fileCollection) {
		this.runtimeClasspath = fileCollection;
	}

	@Override
	public TypeOf<?> getPublicType() {
		return TypeOf.typeOf(LanguageSourceSet.class);
	}

	public SourceDirectorySet create(Language language) {
		String name = language.getSourceDir();
		SourceDirectorySet ret = this.maybeCreate(name);
		SourceDirectorySet all = this.maybeCreate(GUtil.toLowerCamelCase("all " + name));
		all.source(ret);
		all.source(getResources());
		ret.srcDir(new File("src/"+this.getName()+"/"+name));
		ret.setOutputDir(new File(this.getProject().getBuildDir(), name+"/"+this.getName()));
		return ret;
	}

	public SourceDirectorySet getByLanguage(Language language) {
		return getByName(language.getSourceDir());
	}

	public SourceDirectorySet getAllByLanguage(Language language) {
		return getByName(GUtil.toLowerCamelCase("all "+language.getSourceDir()));
	}

	public String getConfigName(String config) {
		String sourceSetName = this.getName();
		String depName = sourceSetName.equals(SourceSet.MAIN_SOURCE_SET_NAME) ? "" : sourceSetName;
		if (!depName.isEmpty() && "default".equals(config.trim().toLowerCase()))
			return depName;
		return GUtil.toLowerCamelCase(depName+" "+config);
	}
}