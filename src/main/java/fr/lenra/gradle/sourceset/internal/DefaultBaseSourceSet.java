package fr.lenra.gradle.sourceset.internal;

import javax.inject.Inject;

import org.gradle.api.file.SourceDirectorySet;
import org.gradle.api.internal.AbstractValidatingNamedDomainObjectContainer;
import org.gradle.api.internal.CollectionCallbackActionDecorator;
import org.gradle.api.internal.project.ProjectInternal;
import org.gradle.api.model.ObjectFactory;
import org.gradle.api.tasks.SourceSet;
import org.gradle.internal.reflect.Instantiator;
import org.gradle.util.GUtil;

import fr.lenra.gradle.sourceset.BaseSourceSet;
import fr.lenra.gradle.sourceset.SourceSetOutput;

public abstract class DefaultBaseSourceSet extends AbstractValidatingNamedDomainObjectContainer<SourceDirectorySet>
		implements BaseSourceSet {
	private final String name;
	private final String displayName;
	private final DefaultSourceSetOutput output;
	private final SourceDirectorySet allSource;
	private final ObjectFactory factory;
	private final ProjectInternal project;

	@Inject
	public DefaultBaseSourceSet(String name, ProjectInternal project, Instantiator instantiator, CollectionCallbackActionDecorator collectionCallbackActionDecorator) {
		super(SourceDirectorySet.class, instantiator, (org.gradle.api.Namer<? super SourceDirectorySet>) new org.gradle.api.Namer<SourceDirectorySet>() {
			public String determineName(SourceDirectorySet ss) {
				return ss.getName();
			}
		}, collectionCallbackActionDecorator);
		this.project = project;
		project.getLogger().info("Create sourceSet "+name);
		this.name = name;

		this.displayName = GUtil.toWords(this.name);
		this.factory = project.getObjects();
		this.output = instantiator.newInstance(DefaultSourceSetOutput.class, project, this);
		this.allSource = project.getObjects().sourceDirectorySet("allSource", "allSource");
	}
	
	@Override
	protected SourceDirectorySet doCreate(String name) {
		project.getLogger().info("Create language source : "+name);
		SourceDirectorySet srcDriSet = factory.sourceDirectorySet(name, this.displayName + " " + GUtil.toWords(name) + " source");
		this.allSource.source(srcDriSet);
		return srcDriSet;
	}

	/**
	 * @return the project
	 */
	protected ProjectInternal getProject() {
		return project;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public SourceSetOutput getOutput() {
		return output;
	}

	@Override
	public SourceSet compiledBy(Object... arg0) {
		return null;
	}

	@Override
	public SourceDirectorySet getAllSource() {
		return allSource;
	}
}