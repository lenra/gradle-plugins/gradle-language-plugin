package fr.lenra.gradle.sourceset.internal;

import java.io.File;

import javax.inject.Inject;

import org.gradle.api.file.SourceDirectorySet;
import org.gradle.api.internal.CollectionCallbackActionDecorator;
import org.gradle.api.internal.project.ProjectInternal;
import org.gradle.internal.reflect.Instantiator;

import fr.lenra.gradle.sourceset.ResourcesSourceSet;

public abstract class DefaultResourcesSourceSet extends DefaultBaseSourceSet implements ResourcesSourceSet {
	private final SourceDirectorySet resources;

	@Inject
	public DefaultResourcesSourceSet(String name, ProjectInternal project, Instantiator instantiator, CollectionCallbackActionDecorator collectionCallbackActionDecorator) {
		super(name, project, instantiator, collectionCallbackActionDecorator);
		this.resources = this.maybeCreate("resources");
		this.resources.srcDir(new File("src/"+this.getName()+"/resources"));
		this.resources.setOutputDir(new File(project.getBuildDir(), "resources/" + this.getName()));
	}

	@Override
	public SourceDirectorySet getResources() {
		return resources;
	}
}