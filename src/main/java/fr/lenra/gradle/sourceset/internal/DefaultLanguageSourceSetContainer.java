package fr.lenra.gradle.sourceset.internal;

import java.util.Collection;

import javax.inject.Inject;

import org.gradle.api.Namer;
import org.gradle.api.internal.AbstractValidatingNamedDomainObjectContainer;
import org.gradle.api.internal.CollectionCallbackActionDecorator;
import org.gradle.api.internal.project.ProjectInternal;
import org.gradle.api.model.ObjectFactory;
import org.gradle.api.reflect.TypeOf;
import org.gradle.internal.reflect.Instantiator;

import fr.lenra.gradle.language.Language;
import fr.lenra.gradle.sourceset.LanguageSourceSet;
import fr.lenra.gradle.sourceset.LanguageSourceSetContainer;

public class DefaultLanguageSourceSetContainer extends AbstractValidatingNamedDomainObjectContainer<LanguageSourceSet> implements LanguageSourceSetContainer {

	private ProjectInternal project;
	private Collection<Language> languages;
	private ObjectFactory objectFactory;

	@Inject
	public DefaultLanguageSourceSetContainer(ObjectFactory objectFactory, Instantiator instantiator, CollectionCallbackActionDecorator collectionCallbackActionDecorator) {
		super(LanguageSourceSet.class, instantiator, (Namer<? super LanguageSourceSet>) new Namer<LanguageSourceSet>() {
			public String determineName(LanguageSourceSet ss) {
				return ss.getName();
			}
		}, collectionCallbackActionDecorator);
		this.objectFactory = objectFactory;
	}

	public void init(ProjectInternal project, Collection<Language> languages) {
		this.project = project;
		this.languages = languages;
	}

	@Override
	protected LanguageSourceSet doCreate(String name) {
		DefaultLanguageSourceSet ret = this.objectFactory.newInstance(DefaultLanguageSourceSet.class, name, languages, project);
		return ret;
	}

	@Override
	public TypeOf<?> getPublicType() {
		return TypeOf.typeOf(LanguageSourceSetContainer.class);
	}
}