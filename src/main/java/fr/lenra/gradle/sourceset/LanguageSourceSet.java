package fr.lenra.gradle.sourceset;

import org.gradle.api.artifacts.Configuration;
import org.gradle.api.file.FileCollection;
import org.gradle.api.file.SourceDirectorySet;

import fr.lenra.gradle.language.Language;

public interface LanguageSourceSet extends ResourcesSourceSet {
	Configuration getCompileClasspath();

	FileCollection getRuntimeClasspath();

	SourceDirectorySet getByLanguage(Language language);
}