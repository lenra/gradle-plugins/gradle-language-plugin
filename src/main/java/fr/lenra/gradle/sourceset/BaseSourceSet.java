package fr.lenra.gradle.sourceset;

import org.gradle.api.NamedDomainObjectContainer;
import org.gradle.api.file.SourceDirectorySet;
import org.gradle.api.tasks.SourceSet;

public interface BaseSourceSet extends NamedDomainObjectContainer<SourceDirectorySet> {

	public abstract String getName();
  
	public abstract SourceSetOutput getOutput();
	
	public abstract SourceSet compiledBy(java.lang.Object... arg0);
  
	public abstract SourceDirectorySet getAllSource();
	
}