package fr.lenra.gradle.sourceset;

import org.gradle.api.file.SourceDirectorySet;

public interface ResourcesSourceSet extends BaseSourceSet {
  
	public abstract SourceDirectorySet getResources();
}